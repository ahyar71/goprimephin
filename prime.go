package main

import (
	"flag"
	"fmt"
	"os"
	"net"
	"net/http"
	"os/signal"
	"syscall"
	"math"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/go-kit/kit/log"
	"github.com/oklog/run"
	"github.com/elithrar/admission-control"
)

const (
	default_port    string = ":8080"
)

var (
	port    = flag.String("port", default_port, "TCP port of service")
)

func homeLink(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Home. Gunakan /prime/{number} untuk cek bilangan prima")
}

func checkPrimeNumber(num int) bool {
	if num < 2 {
	   fmt.Println("Number must be greater than 2.")
	   return false
	}
	sq_root := int(math.Sqrt(float64(num)))
	for i:=2; i<=sq_root; i++{
	   if num % i == 0 {
		  return false
	   }
	}
	return true
 }

func primeLink(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	n, err := strconv.Atoi(vars["number"])
	if err != nil || n < 2 {
		message :=  "Masukan salah."
		fmt.Fprintf(w, message)
	} else if checkPrimeNumber(n) {
		message :=  vars["number"] + " is prime"
		fmt.Fprintf(w, message)
	} else {
		message :=  vars["number"] + " is not prime"
		fmt.Fprintf(w, message)
	}
}

func main() {
	var g run.Group
	var logger log.Logger

	flag.Parse()

	logger = log.NewJSONLogger(log.NewSyncWriter(os.Stdout))
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)

	{
		// Handle system interrupts
		cancel := make(chan struct{})
		g.Add(
			func() error {
				c := make(chan os.Signal, 1)
				signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
				select {
				case sig := <-c:
					return fmt.Errorf("received signal %s", sig)
				case <-cancel:
					return nil
				}
			},
			func(error) {
				close(cancel)
			})
	}

	{
		// HTTP listener
		listener, err := net.Listen("tcp", *port)
		if err != nil {
			logger.Log("during", "Listen", "error", err)
			os.Exit(1)
		}
		g.Add(
			func() error {
				logger.Log("event", "start", "port", *port)
				router := mux.NewRouter().StrictSlash(true)
				router.HandleFunc("/", homeLink)
				router.HandleFunc("/prime/{number}", primeLink)
				loggingMiddleware := admissioncontrol.LoggingMiddleware(logger)
				loggedRouter := loggingMiddleware(router)
				return http.Serve(listener, loggedRouter)
			},
			func(error) {
				listener.Close()
			})
	}
	
	logger.Log("event", "stop", "exit", g.Run())
}
